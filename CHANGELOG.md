# Changelog

This changelog includes all the changes that have been made to the project over time. 

## Unreleased

## v1.1.4 - Hello again
- Updated
  - README file to reflect modern changes
  
## v1.1.33L - Patching things up
- Added
  - LayeredFS release for the consoles with Vapecord included

- Changed
  - The titlescreen elements such as the logo and metadata for the LayeredFS release to match each version of Animal Crossing 

- Improved
  - Compatibility for various versions of Animal Crossing 

## v1.1.33 - First Edits
- Added
  - Scarecrow PWP
  - Ability to gain playcoins when buying fortune cookies
  - No limit in shampoodle 

- Fixed
  - Volume issue with "Victory Fanfare"
  - Intro dialogue that was heavily bugged 

- Removed
  - First letter from turtle
  - First Bulletin Board notice 

## v1.0.0 - Welcome Luxury
- Added
  - Ability to take items to the island through the coolerbox

- Removed
  - The "Move In" or "Import Town" buttons since they won't work for ACWL

- Changed
  - Reworked snow textures for various elements in the game 

## Pre-v0.0.6 - The Small Things
- Added
  - Black bells 
  - Goldapples 
  - More building songs and jingles
  - New backgrounds 
  - New cursor 
  - New default font 

## Pre-v0.0.596 - Snow Fix
- Added
  - Captions to the releases in the changelog file

- Changed
  - Adjusted all songs to be equally loud

- Fixed
  - Snowing song issue 

- Removed
  - Camera zoom 

## Pre-v0.0.5 - PWP Changes
- Changed
  - Several PWP elements:
    - Archway Sculpture
    - Balloon Arch
    - Brick Bridge
    - Camping Cot
    - Campsite 
    - Cobblestone Bridge
    - Fairy-Tale Bridge
    - Flower Arch
    - Hammock
    - Illuminated Arch
    - Modern Bridge
    - Outdoor Chair
    - Solar Panels
    - Suspension Bridge
    - Wind Turbine
    - Wisteria Trellis
    - Wooden Bridge

## Pre-v0.0.5 - PWP Changes
- Added:
    - Camera zoomed slightly out
    - Cyrus customizations are now done instantly
    - Fish now bite on the first try and do not vanish when running
    - Flowers (except black roses) never wilt
    - Mayor Permit is now instantly 100% complete
    - New default button background
- Changed:
    - PWPs:
        - Archway Sculpture
        - Balloon Arch
        - Bell
        - Blue Bench
        - Brick Bridge
        - Bus Stop
        - Camping Cot
        - Campsite
        - Caution Sign
        - Chair Sculpture
        - Circle Topiary
        - Cobblestone Bridge
        - Cube Sculpture
        - Custom-Design Sign
        - Do-Not-Enter Sign
        - Drilling Rig
        - Drinking Fountain
        - Face-Cutout Standee
        - Fairy-Tale Bench
        - Fairy-Tale Bridge
        - Fairy-Tale Clock
        - Fairy-Tale Streetlight
        - Fence
        - Fire Hydrant
        - Fire Pit
        - Flower Arch
        - Flower Bed
        - Flower Clock
        - Fountain
        - Garbage Can
        - Geyser
        - Hammock
        - Hot Spring
        - Illuminated Arch
        - Illuminated Clock
        - Illuminated Heart
        - Illuminated Tree
        - Instrument Shelter
        - Jungle Gym
        - Lighthouse
        - Log Bench
        - Metal Bench
        - Moai Statue
        - Modern Bench
        - Modern Bridge
        - Modern Clock
        - Modern Streetlight
        - Outdoor Chair
        - Parabolic Antenna
        - Parkclock
        - Picnic Blanket
        - Pile of Pipes
        - Pyramid
        - Rack of Rice
        - Round Streetlight
        - Sandbox
        - Scarecrow
        - Solar Panels
        - Sphinx
        - Square Topiary
        - Stadium Light
        - Statue Fountain
        - Stone Tablet
        - Stonehenge
        - Street Lamp
        - Streetlight
        - Suspension Bridge
        - Tire Toy
        - Torch
        - Totem Pole
        - Tower
        - Traffic Signal
        - Tulip Topiary
        - Video Screen
        - Water Pump
        - Water well
        - Wind Turbine
        - Windmill
        - Wisteria Trellis
        - Wood Bench
        - Wooden Bridge
        - Yellow Bench
        - Yield Sign
        - Zen Bell
        - Zen Bench
        - Zen Clock
        - Zen Garden
        - Zen Streetlight
    - Train Station exteriors:
        - Basic Train Station Ex
        - Eastern Train Station Ex
        - Fairy Tale Train Station Ex
        - Modern Train Station Ex

## Pre-v0.0.4 - Details
- Added: 
    - New TID
    - Songs (Credits can be found in ExtractedRomFs/Sound/stream/Credit.md)
- Changed:
    - Exterior designs:
        - Base Townhall Ex
        - Igloo Ex
        - ReTail Ex
        - Train Ex
    - PWPs:
        - Boat
        - Bulletin Board
        - Buoy
        - Cooler Box
        - Crossing
        - Fences
        - Sewing Machine
    - Game version to ACWL and dark icon

## Pre-v0.0.3 - The Overhaul
Added:
- '© 2019 Kyusetzu' to the Titlescreen
- 24h Music + Indoor Songs
- Credits for every song added in ExtractedRomFs/Sound/stream/Credit.md
- Loading Icon
- Custom Item Icons
- Custom Train Dialogue
- Exterior:
    - Townhall Ex
Changed:
- Exterior:
    - Overhauled everything
- Interior:
    - Overhauled everything

## Pre-v0.0.2 - Exterior Design
Added:
- Bench
- Lamps
- Stopper
- Bannersound
- Golden Flying Present
- Golden Townsapling when hold in the Cutscene
- Luxurious Gold & Silver Toolbox
- Updatefile

Changed:
- Exterior:
    - Accessory Shop Ex
    - Barber Ex
    - Dreamhouse Ex
    - Flowershop Ex
    - Museum Ex
    - Nook's Shop Ex
    - Nookling Shops Ex
    - Photobox Ex
    - Post Office Ex
    - Shoeshop Ex
- Interior:
    - Changed "Red Carpet Start"
    - Luxury-Black Market
    - Luxury-Grace
    - Luxury-Island Hut
    - Luxury-Museum
    - Luxury-Museum Shop
    - Luxury-Nook's Shop
    - Luxury-Nookling Shops
    - Luxury-Postoffice
    - Luxury-Reset Center
    - Luxury-ReTail
    - Luxury-Tailor
    - Luxury-Tent (Fish/Insect)
- PWPs & Other:
    - Disabled Resetti
    - Fruits are sold for foreign price
    - Mosquitos never bite
    - Stores are open 24/7
    - Unbreakable Flowers

## Pre-v0.0.1 - Basics and Interior Design
Added:
- Custom Metadata & Banner
- Golden Feather
- Golden Polar Lights
- Golden Ticketmachine & Townsapling
- Golden Triangle Shades
- Misc. Objects for Cafe
- Rainbowroses

Changed:
- Birds to white
- "Red Carpet Start"
- "Welcome Luxury" Titlescreen
- Interior:
    - Luxury-Accessory Shop
    - Luxury-Barber
    - Luxury-Cafe
    - Luxury-ClubLOL
    - Luxury-Dreamhouse
    - Luxury-Flowershops
    - Luxury-Igloo
    - Luxury-Police Stations
    - Luxury-Shoeshop
    - Luxury-Townhalls
    - Luxury-Train
    - Luxury-Train Stations
