# ![Logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11357955/Logo_2.png) Animal Crossing: Welcome Luxury

``Animal Crossing: Welcome Luxury`` is a ROM-Hack based on ``Animal Crossing: New Leaf Welcome Amiibo`` that introduces various changes to the gameplay experience.

## Changes

<details class="spoiler">
<summary>Click here to see the full list of changes!</summary>
- Fish now bite on the first try
- Fish do not disappear when you run up to them
- Flowers never wilt (except black roses)
- Playcoins are gained when buying fortunecookies
- Flowers are now indestructible
- Cyrus customizations are now instant
- Mayor permit is now instant
- Mosquitoes never bite
- New font
- New music
- No tripping
- QR machine is unlocked
- Resetti is disabled
- Textures have been reworked
- Stores are open 24/7
- Items can be taken to the island through the coolerbox
- The price of foreign fruits is now the same as the town-fruit
- There are unlimited visits to Shampoodle

[For a detailed changelog, click here.](CHANGELOG.md)

</details>

## Download

You can download ``Animal Crossing: Welcome Luxury`` from the following sources:

- Go to the [Releases Page](https://gitlab.com/Kyusetzu/ACWL/-/releases) and carefully read the information provided there.

or 

- Go to the [hShop Page](https://hshop.erista.me/search/results?q=Welcome+Luxury&qt=Text&lgy=false) and download both files.

***Please be aware that updating ``Animal Crossing: Welcome Luxury`` may delete your saved data, so it's a good idea to create a backup before updating.***

## More Information

For additional information on ``Animal Crossing: Welcome Luxury``, please visit:

- The [Wiki](https://gitlab.com/Kyusetzu/ACWL/-/wikis/home)
- Our [Discord channel](https://discord.gg/yeHNSGyM8F)

## Acknowledgments

Animal Crossing: Welcome Luxury was made possible thanks to the contributions and support of the following individuals:

- Slattz#0674 for help with small tasks, providing information, and adding features with the REditor.
- Lukas#4444 for assistance with mod to code implementation.
- Sarachi#2261 for ideas, verification, and wiki-management.
- Jason#9789 for alpha and beta testing.
- ~Harsy~#7126 for providing wall and floor fixes.

###### Wiki Translation:

- German || Kyusetzu#0861 & Sarachi#2261
- Spanish || im a book#5877
- French || Youssef#5154
- Portuguese || AdSF#2943 & Davidfaisca7#8535
- Italian || Fedecrash02#2418
- Dutch || nuisance®#0245